# CivicMinded #



### What is CivicMinded? ###

**CivicMinded** is an app that collects municipal, state, and federal information as a guide to voting and elections; 
immigration reform; and the **US** citizenship test process.

Immigrants are helped with the process of becoming **US** citizens with citizenship test aids.